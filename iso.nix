{ nixpkgs ? <nixpkgs> , system ? "x86_64-linux" }:
let
	isoconfig = { pkgs, ... }: {
		imports = [
			"${nixpkgs}/nixos/modules/installer/cd-dvd/iso-image.nix"
			"${nixpkgs}/nixos/modules/installer/cd-dvd/channel.nix"
	
      			./boot.nix
      			./packages.nix
	      		./users.nix
      			./locale.nix
      			./networking.nix
	      		./services.nix
      			./sound.nix
      			./xserver.nix
		];
	};

	evalNixos = configuration: import "${nixpkgs}/nixos" {
		inherit system configuration;
	};

in { iso = (evalNixos isoconfig).config.system.build.isoImage; }
