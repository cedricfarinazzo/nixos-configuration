
{ config, lib, pkgs, modulesPath, ... }:

{
    services.xserver = {
	enable = true;
	autorun = true;
	layout = "fr";
	xkbOptions = "eurosign:e";

	desktopManager = {
	    xterm.enable = true;
	};

	displayManager = {
	    lightdm.enable = true;
	    defaultSession = "none+i3";
	};
	
	windowManager = {
		i3.enable = true;
	};

        # Enable touchpad support (enabled default in most desktopManager).
        libinput.enable = true;
    };
}
