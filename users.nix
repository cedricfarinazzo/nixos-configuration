{ config, lib, pkgs, modulesPath, ... }:

{
	security.sudo.enable = true;

	users.users = {
#		root.hashedPassword = ""		
		sed = {
			isNormalUser = true;
			home = "/home/sed";
#			hashedPassword = "";
			extraGroups = [ "wheel" "networkmanager" ];
		};
	};
}
