{ config, pkgs, ... }:

{
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Disable the firewall.
  # networking.firewall.enable = false;
  
  # Enable CUPS to print documents.
  # services.printing.enable = true;
}
